import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
from torch.nn.utils import spectral_norm
import math


class SelfAttn(nn.Module):

    def __init__(self,  input_dim , activation=nn.ReLU, downsample=8):
        super(SelfAttn,self).__init__()
        self.input_dim = input_dim
        self.activation = activation
        self.q = nn.Conv2d(input_dim, input_dim//downsample, kernel_size=1)
        self.k = nn.Conv2d(input_dim, input_dim//downsample, kernel_size=1)
        self.v = nn.Conv2d(input_dim, input_dim, kernel_size=1)
        self.gamma = nn.Parameter(torch.zeros(1))
        self.softmax  = nn.Softmax(dim=-1)
    
    def forward(self, x):
        B, C, H, W = x.shape
        proj_key =  self.k(x).view(B, -1, W*H)
        proj_query  = self.q(x).view(B, -1, W*H).permute(0,2,1) # B X CX(N)
        energy =  torch.bmm(proj_query,proj_key) # transpose check
        attention = self.softmax(energy) # BX (N) X (N) 
        proj_value = self.v(x).view(B,-1,W*H) # B X C X N

        out = torch.bmm(proj_value,attention.permute(0,2,1) )
        out = out.view(B, C, W, H)        
        out = self.gamma*out + x
        return out, attention


class MultiHead(nn.Module):

    def __init__(self, input_dim, num_heads=4, activation=nn.ReLU, downsample=8):
        super(MultiHead, self).__init__()
        self.downsample = nn.Conv2d(input_dim*num_heads, input_dim, kernel_size=1)
        heads = []

        for idx in range(num_heads):
            heads.append( SelfAttn(input_dim, activation=activation, downsample=downsample) )
        self.heads = nn.ModuleList(heads)
        self.num_heads = num_heads

    def forward(self, x):
        outputs = []
        attentions  = []
        for idx in range(self.num_heads):
            out, attn = self.heads[idx](x)
            outputs.append(out)
            attentions.append(attn)

        output =  torch.cat(outputs, dim=1)
        return self.downsample(output)


class ResGenBlock(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(ResGenBlock, self).__init__()
        self.block = nn.Sequential(
            nn.BatchNorm2d(input_dim),
            nn.ReLU(),
            nn.Upsample(scale_factor=2),
            spectral_norm(nn.Conv2d(input_dim, output_dim, 3, stride=1, padding=1)),
            nn.BatchNorm2d(output_dim),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(output_dim, output_dim, 3, stride=1, padding=1)),
        )
        self.shortcut = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.Conv2d(input_dim, output_dim, 1, stride=1, padding=0)
        )

    def forward(self, x):
        return self.block(x) + self.shortcut(x)


def res_arch_init(model):
    for name, module in model.named_modules():
        if isinstance(module, (nn.Conv2d, nn.ConvTranspose2d)):
            if 'residual' in name:
                init.xavier_uniform_(module.weight, gain=math.sqrt(2))
            else:
                init.xavier_uniform_(module.weight, gain=1.0)
            if module.bias is not None:
                init.zeros_(module.bias)
        if isinstance(module, nn.Linear):
            init.xavier_uniform_(module.weight, gain=1.0)
            if module.bias is not None:
                init.zeros_(module.bias)

class Generator(nn.Module):

    def __init__(self, z_dim=100, num_heads=2, h_dim=256, use_attn=False):
        super(Generator, self).__init__()      
        self.h_dim = h_dim
        self.linear = nn.Linear(z_dim, 4 * 4 * h_dim)
        if use_attn:
            self.attn1 = MultiHead( h_dim, num_heads=num_heads, downsample=8 )
            self.attn2 = MultiHead( h_dim, num_heads=min(num_heads//2, 1), downsample=32 )
        self.use_attn = use_attn
        self.block_1 = nn.Sequential(
            ResGenBlock(h_dim, h_dim),
            ResGenBlock(h_dim, h_dim)
        )

        self.block_2 = ResGenBlock(h_dim, h_dim)

        self.output_blocks = nn.Sequential(
            nn.BatchNorm2d(h_dim),
            nn.ReLU(),
            nn.Conv2d(h_dim, 3, 3, stride=1, padding=1),
            nn.Tanh(),
        )
        res_arch_init(self)

    def forward(self, z):
        # z : batch size x z_dim
        x = self.linear(z).view(-1, self.h_dim, 4, 4)
        x = self.block_1(x)
        if self.use_attn:
            x = self.attn1(x)
        x = self.block_2(x)
        if self.use_attn:
            x = self.attn2(x)
        x = self.output_blocks(x)
        return x


class ResDisBlock(nn.Module):
    def __init__(self, in_channels, out_channels, down=False):
        super().__init__()
        shortcut = []
        if in_channels != out_channels or down:
            shortcut.append(spectral_norm(
                nn.Conv2d(in_channels, out_channels, 1, 1, 0)))
        if down:
            shortcut.append(nn.AvgPool2d(2))
        self.shortcut = nn.Sequential(*shortcut)

        residual = [
            nn.ReLU(),
            spectral_norm(nn.Conv2d(in_channels, out_channels, 3, 1, 1)),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(out_channels, out_channels, 3, 1, 1)),
        ]
        if down:
            residual.append(nn.AvgPool2d(2))
        self.residual = nn.Sequential(*residual)

    def forward(self, x):
        return (self.residual(x) + self.shortcut(x))

class OptimizedResDisblock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.shortcut = nn.Sequential(
            nn.AvgPool2d(2),
            spectral_norm(nn.Conv2d(in_channels, out_channels, 1, 1, 0)))
        self.residual = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels, out_channels, 3, 1, 1)),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(out_channels, out_channels, 3, 1, 1)),
            nn.AvgPool2d(2))

    def forward(self, x):
        return self.residual(x) + self.shortcut(x)


class Discriminator(nn.Module):
    def __init__(self, num_heads=2, use_attn=False):
        super(Discriminator, self).__init__()
        self.use_attn = use_attn
        if use_attn:
            self.attn = MultiHead( 128, num_heads=num_heads )
        self.first_stage = nn.Sequential(
            OptimizedResDisblock(3, 128),
            ResDisBlock(128, 128, down=True),
            ResDisBlock(128, 128)
        )
        self.model = nn.Sequential(
            ResDisBlock(128, 128),
            nn.ReLU())
        self.linear = spectral_norm(nn.Linear(128, 1, bias=False))
        res_arch_init(self)

    def forward(self, x):
        x = self.first_stage(x)
        if self.use_attn:
            x = self.attn(x)

        x = self.model(x).sum(dim=[2, 3])
        x = self.linear(x)
        return x

if __name__ == "__main__":
    x = torch.rand(32, 100)
    gen = Generator()
    dis = Discriminator()
    x = gen(x)
    y = dis(x)
    print(x.shape, y.shape)