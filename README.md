# Spectral Multi-head attention GAN


## Improvements

* Added Multi head attention instead of single self attention ( more is better right?)

    * Directly compare the same hyperparam to SNGAN, multi head is slower and worse in performance

    * Stability requires more tuning

* Custom Adam which doesn't store model's momentum state because we always use (0.0, 0.9) as the beta parameters

    * Seems like pytorch F.Adam contain optimization code which prevents allocation when beta1 (momentum) is 0

    * Performance wise there's no difference between the two

# Setup

```
git clone https://github.com/theblackcat102/smagan.git
cd smagan
git submodule update --init --recursive
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt
```

# Train

Train on [CODEBRIM: COncrete DEfect BRidge IMage Dataset ](https://zenodo.org/record/2620293) crops (defects) images

You can change *codebrim* to *cifar10* to train on cifar10 evaluation

```
python train.py --logdir ./logs/SNGAN+CR-0.5_CODEBRIM_RES_large \
     --lr_D 3e-4 --lr_G 1e-4 \
     --dataset codebrim \
     --fid_cache ./stats/codebrim.npz \
     --consistency_regul \
     --batch_size 128 \
     --n_dis 1 \
     --consistency_weight 0.5
```


```
python train.py \
    --generate \
    --pretrain ./logs/SNGAN+CR-0.05_CODEBRIM_RES_large/model.pt \
    --num_images 50000
```