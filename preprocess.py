from zipfile import ZipFile
import zipfile
import torch
import os
from torchvision import datasets, transforms
from PIL import Image
import PIL

file_name = '/mnt/6b644ef8-bba1-4b5f-8d44-7194d942faf0/CODEBRIM_cropped_dataset.zip'

if __name__ == '__main__':

    # with ZipFile(file_name, 'r') as zip:
    #     output = zip.namelist()
    #     for filename in output:
    #         try:
    #             if '__MACOSX/' not in filename and '.png' in filename:
    #                 zip.extract(filename)

    #                 pil_image = Image.open(filename)
    #                 pil_image_rgb = pil_image.convert('RGB')
    #                 pil_image_rgb.save(filename.replace('.png', '.jpg'), format='JPEG', quality=90)
    #                 os.remove(filename)

    #         except zipfile.BadZipFile:
    #             continue

    dataset = datasets.ImageFolder('cropped_dataset', transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.RandomResizedCrop((32, 32)),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            transforms.Lambda(lambda x: x + torch.rand_like(x) / 128)
        ]))
    from tqdm import tqdm
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=64, shuffle=True, num_workers=4,
        drop_last=True)
    for batch in tqdm(dataloader):
        batch[0].shape
    # print(dataset[0])